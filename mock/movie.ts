import { MockMethod } from 'vite-plugin-mock'
// 电影管理相关接口
import { getFileContext, doPagination, responseStatus, getDataByKey } from './util'
import { movieAreaFile, movieTypeFile, movieFile } from './filePath'
export default [
  // 获取电影列表
  {
    url: '/api/movie/movieList',
    method: 'get',
    timeout: 500,
    response: ({ query }) => {
      const data = getFileContext(movieFile)
      const res = doPagination(data.reverse(), query.page, query.pageSize)
      return {
        ...responseStatus(),
        data: res,
        total: data.length
      }
    }
  },
  {
    url: '/api/movie/movieInfo',
    method: 'get',
    timeout: 200,
    response: ({ query }) => {
      const data = getFileContext(movieFile)
      const res = getDataByKey(data, Number(query.id))
      if (res) {
        return {
          ...responseStatus(),
          data: res
        }
      } else {
        return {
          ...responseStatus(2)
        }
      }
    }
  },
  // 获取地区
  {
    url: '/api/movie/movieAreaList',
    method: 'get',
    timeout: 200,
    response: () => {
      const res = getFileContext(movieAreaFile)
      return {
        ...responseStatus(),
        data: res.reverse(),
        total: res.length
      }
    }
  },
  // 获取类型
  {
    url: '/api/movie/movieTypeList',
    method: 'get',
    timeout: 200,
    response: () => {
      const res = getFileContext(movieTypeFile)
      return {
        ...responseStatus(),
        data: res.reverse(),
        total: res.length
      }
    }
  }
] as MockMethod[]
