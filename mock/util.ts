// 一些通用方法/配置

import fs from 'fs'
// 获取json数据
export function getFileContext (filePath: string): [] {
  return JSON.parse(fs.readFileSync(__dirname + filePath, { encoding: 'utf-8' }) || '[]')
}

// 保存json数据
export function setFileContext (filePath: string, context: [] = []) {
  return fs.writeFileSync(__dirname + filePath, JSON.stringify(context), { encoding: 'utf-8' })
}

// 根据id或者其他条件返回对应数据
export function getDataByKey (data, value, key = 'id') {
  const res = data.find((item) => value === item[key])
  return res
}
// 根据id新增一条数据
export function addOneData (data, pushData) {
  pushData.id = data.length ? Number(data[data.length - 1].id) + 1 : 1
  data.push(pushData)
}
// 更新一条数据
export function updateOneData (data, updateData) {
  const index = data.findIndex(item => Number(item.id) === Number(updateData.id))
  data[index] = updateData
}
// 删除一条数据
export function removeOneData (data, id) {
  const index = data.findIndex(item => Number(item.id) === Number(id))
  data.splice(index, 1)
}

// 分页
export function doPagination (data = [], page = 1, size = 10) {
  const start = --page * Number(size)
  return data.slice(start, start + Number(size))
}

// 通用状态码
export function responseStatus (i = 1) {
  const s = {
    1: {
      code: 1,
      msg: '请求成功'
    },
    2: {
      code: 2,
      msg: '请求失败'
    },
    3: {
      code: 3,
      msg: '未登录'
    }
  }
  return s[i]
}
