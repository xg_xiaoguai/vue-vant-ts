import axios from 'axios'
import type { InternalAxiosRequestConfig } from 'axios'
import { showLoadingToast } from 'vant';
let msgCtrl: any = null
const request = axios.create({
  baseURL: '/api',
  timeout: 10000
})
export type otherConfig = {
  hideLoading?: boolean
}
// 请求拦截
request.interceptors.request.use((config: InternalAxiosRequestConfig<{}> & otherConfig) => {
  if(!config.hideLoading) msgCtrl = showLoadingToast('加载中')
  // 发送请求前处理
  config.headers.tokenId = localStorage.getItem('tokenId')
  return config
}, (err) => {
  // 请求失败处理
  return Promise.reject(err)
})

// 响应拦截
request.interceptors.response.use((res) => {
  console.log(res.data)
  msgCtrl && msgCtrl.close()
  msgCtrl = null // 还原初始值
  const { data } = res
  if(data.code === 1) {
    return res.data
  } else {
    return Promise.reject(data)
  }
}, (err) => {
  return Promise.reject(err)
})

export default request