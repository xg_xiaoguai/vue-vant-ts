import axios from '../axios-config'
import type { InternalAxiosRequestConfig } from 'axios'
import type { otherConfig } from '../axios-config'
export function getMovieList(params?: {}) {
  return axios({
    url: '/movie/movieList',
    method: 'get',
    params,
    hideLoading: true
  } as InternalAxiosRequestConfig & otherConfig)
}