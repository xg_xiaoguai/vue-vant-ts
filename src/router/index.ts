import { createRouter, createWebHashHistory } from "vue-router";
import type { RouteRecordRaw } from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'root',
        redirect: 'index'
    },
    {
        path: '/index',
        name: 'index',
        component: () => import('@/views/home/HomeIndex.vue')
    },
    {
        path: '/detail/:id(\\d+)',
        name: 'detail',
        component: () => import('@/views/home/detail.vue')
    }
] as RouteRecordRaw[]

export const router = createRouter({
    history: createWebHashHistory(),
    routes,
})