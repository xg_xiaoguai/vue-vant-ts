import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteMockServe } from 'vite-plugin-mock'
import components from 'unplugin-vue-components/vite'
import { VantResolver } from 'unplugin-vue-components/resolvers'
import eslintPlugin from 'vite-plugin-eslint'
// https://vitejs.dev/config/
export default ({ command }) => defineConfig({
  plugins: [
    vue(),
    viteMockServe({
      mockPath: 'mock',
      localEnabled: command === 'serve',
    }),
    components({
      resolvers: [VantResolver()]
    }),
    eslintPlugin()
  ],
  resolve: {
    alias: {
      '@': '/src'
    }
  },
  server: {
    port: 1992,
    // proxy: {
    //   '/api': {
    //     target: 'http://127.0.0.1:2020',
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace('/api', '')
    //   }
    // }
  }
})
