# Vue 3 + TypeScript + Vite
一个 vite + vue3 + typescript + vant+ router + axios + mock + eslint 移动端基础模板，并包含一些通用组件和方法

## 获取代码
```
git clone https://gitee.com/xg_xiaoguai/vue-vant-ts-template.git
```

## 进入项目
```
cd vue-vant-ts-template
```

## 安装依赖
```
npm install
```

## 开发运行
```
npm run dev
```

## 打包部署
```
npm run build
```

## 项目预览
需要先打包，再预览
```
npm run preview
```

#### 页面展示
![列表](https://gitee.com/xg_xiaoguai/xg-imange/raw/master/vant-ts-template/101.png)
![表单](https://gitee.com/xg_xiaoguai/xg-imange/raw/master/vant-ts-template/102.png)
![表格](https://gitee.com/xg_xiaoguai/xg-imange/raw/master/vant-ts-template/103.png)
![筛选](https://gitee.com/xg_xiaoguai/xg-imange/raw/master/vant-ts-template/104.png)
